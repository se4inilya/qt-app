/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionCreate;
    QAction *actionOpen;
    QAction *actionImport;
    QAction *actionExport;
    QAction *actionExit;
    QWidget *centralWidget;
    QPushButton *update_button;
    QPushButton *delete_button;
    QPushButton *create_button;
    QListWidget *listWidget;
    QLabel *label_5;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *author_label;
    QLabel *id_label;
    QLabel *char_label;
    QLabel *book_label;
    QLabel *label_6;
    QLineEdit *search_field;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *previous_button;
    QLabel *page_label;
    QPushButton *next_button;
    QMenuBar *menuBar;
    QMenu *menuMenu;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1124, 426);
        actionCreate = new QAction(MainWindow);
        actionCreate->setObjectName(QStringLiteral("actionCreate"));
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionImport = new QAction(MainWindow);
        actionImport->setObjectName(QStringLiteral("actionImport"));
        actionExport = new QAction(MainWindow);
        actionExport->setObjectName(QStringLiteral("actionExport"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        update_button = new QPushButton(centralWidget);
        update_button->setObjectName(QStringLiteral("update_button"));
        update_button->setGeometry(QRect(60, 10, 121, 51));
        delete_button = new QPushButton(centralWidget);
        delete_button->setObjectName(QStringLiteral("delete_button"));
        delete_button->setGeometry(QRect(340, 10, 121, 51));
        create_button = new QPushButton(centralWidget);
        create_button->setObjectName(QStringLiteral("create_button"));
        create_button->setGeometry(QRect(600, 10, 121, 51));
        create_button->setCheckable(false);
        create_button->setAutoDefault(false);
        create_button->setFlat(false);
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(20, 140, 256, 192));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(400, 120, 131, 17));
        formLayoutWidget = new QWidget(centralWidget);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(310, 150, 451, 101));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        label_4 = new QLabel(formLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        author_label = new QLabel(formLayoutWidget);
        author_label->setObjectName(QStringLiteral("author_label"));

        formLayout->setWidget(2, QFormLayout::FieldRole, author_label);

        id_label = new QLabel(formLayoutWidget);
        id_label->setObjectName(QStringLiteral("id_label"));

        formLayout->setWidget(0, QFormLayout::FieldRole, id_label);

        char_label = new QLabel(formLayoutWidget);
        char_label->setObjectName(QStringLiteral("char_label"));

        formLayout->setWidget(3, QFormLayout::FieldRole, char_label);

        book_label = new QLabel(formLayoutWidget);
        book_label->setObjectName(QStringLiteral("book_label"));

        formLayout->setWidget(1, QFormLayout::FieldRole, book_label);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(800, 40, 291, 271));
        search_field = new QLineEdit(centralWidget);
        search_field->setObjectName(QStringLiteral("search_field"));
        search_field->setGeometry(QRect(20, 340, 261, 25));
        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(20, 90, 259, 41));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        previous_button = new QPushButton(horizontalLayoutWidget);
        previous_button->setObjectName(QStringLiteral("previous_button"));

        horizontalLayout->addWidget(previous_button);

        page_label = new QLabel(horizontalLayoutWidget);
        page_label->setObjectName(QStringLiteral("page_label"));

        horizontalLayout->addWidget(page_label);

        next_button = new QPushButton(horizontalLayoutWidget);
        next_button->setObjectName(QStringLiteral("next_button"));

        horizontalLayout->addWidget(next_button);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1124, 22));
        menuMenu = new QMenu(menuBar);
        menuMenu->setObjectName(QStringLiteral("menuMenu"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuMenu->menuAction());
        menuMenu->addAction(actionImport);
        menuMenu->addAction(actionExport);
        menuMenu->addAction(actionExit);

        retranslateUi(MainWindow);

        create_button->setDefault(false);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "BookStorage", Q_NULLPTR));
        actionCreate->setText(QApplication::translate("MainWindow", "Create", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionCreate->setShortcut(QApplication::translate("MainWindow", "Ctrl+N", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionOpen->setText(QApplication::translate("MainWindow", "Open", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionOpen->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionImport->setText(QApplication::translate("MainWindow", "Import", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionImport->setShortcut(QApplication::translate("MainWindow", "Ctrl+Q", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionExport->setText(QApplication::translate("MainWindow", "Export", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionExport->setShortcut(QApplication::translate("MainWindow", "Ctrl+W", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionExit->setText(QApplication::translate("MainWindow", "Exit", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionExit->setShortcut(QApplication::translate("MainWindow", "End", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        update_button->setText(QApplication::translate("MainWindow", "Update", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        update_button->setShortcut(QApplication::translate("MainWindow", "Ctrl+U", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        delete_button->setText(QApplication::translate("MainWindow", "Delete", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        delete_button->setShortcut(QApplication::translate("MainWindow", "Del", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        create_button->setText(QApplication::translate("MainWindow", "Create", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        create_button->setShortcut(QApplication::translate("MainWindow", "Ctrl+E", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_ACCESSIBILITY
        listWidget->setAccessibleName(QString());
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        listWidget->setAccessibleDescription(QString());
#endif // QT_NO_ACCESSIBILITY
        label_5->setText(QApplication::translate("MainWindow", "Selected Element", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Id :", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "Book name :", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "Author name :", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Character capacity :", Q_NULLPTR));
        author_label->setText(QString());
        id_label->setText(QString());
        char_label->setText(QString());
        book_label->setText(QString());
        label_6->setText(QString());
        search_field->setPlaceholderText(QApplication::translate("MainWindow", "Field for search", Q_NULLPTR));
        previous_button->setText(QApplication::translate("MainWindow", "Previous", Q_NULLPTR));
        page_label->setText(QString());
        next_button->setText(QApplication::translate("MainWindow", "Next", Q_NULLPTR));
        menuMenu->setTitle(QApplication::translate("MainWindow", "Menu", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
