/****************************************************************************
** Meta object code from reading C++ file 'updatedialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../src/course_work/updatedialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'updatedialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_UpdateDialog_t {
    QByteArrayData data[16];
    char stringdata0[270];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_UpdateDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_UpdateDialog_t qt_meta_stringdata_UpdateDialog = {
    {
QT_MOC_LITERAL(0, 0, 12), // "UpdateDialog"
QT_MOC_LITERAL(1, 13, 17), // "sendUpdatedSignal"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 5), // "Book*"
QT_MOC_LITERAL(4, 38, 11), // "sendStorage"
QT_MOC_LITERAL(5, 50, 8), // "Storage*"
QT_MOC_LITERAL(6, 59, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(7, 81, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(8, 105, 17), // "receiveBookToEdit"
QT_MOC_LITERAL(9, 123, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(10, 140, 24), // "on_insert_button_clicked"
QT_MOC_LITERAL(11, 165, 24), // "on_delete_button_clicked"
QT_MOC_LITERAL(12, 190, 25), // "on_listWidget_itemClicked"
QT_MOC_LITERAL(13, 216, 4), // "item"
QT_MOC_LITERAL(14, 221, 24), // "receiveupdatedAuthorList"
QT_MOC_LITERAL(15, 246, 23) // "on_pushButton_3_clicked"

    },
    "UpdateDialog\0sendUpdatedSignal\0\0Book*\0"
    "sendStorage\0Storage*\0on_pushButton_clicked\0"
    "on_pushButton_2_clicked\0receiveBookToEdit\0"
    "QListWidgetItem*\0on_insert_button_clicked\0"
    "on_delete_button_clicked\0"
    "on_listWidget_itemClicked\0item\0"
    "receiveupdatedAuthorList\0"
    "on_pushButton_3_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_UpdateDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   64,    2, 0x06 /* Public */,
       4,    2,   67,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,   72,    2, 0x08 /* Private */,
       7,    0,   73,    2, 0x08 /* Private */,
       8,    2,   74,    2, 0x08 /* Private */,
      10,    0,   79,    2, 0x08 /* Private */,
      11,    0,   80,    2, 0x08 /* Private */,
      12,    1,   81,    2, 0x08 /* Private */,
      14,    1,   84,    2, 0x08 /* Private */,
      15,    0,   87,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 5, 0x80000000 | 3,    2,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 9, 0x80000000 | 5,    2,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 9,   13,
    QMetaType::Void, 0x80000000 | 5,    2,
    QMetaType::Void,

       0        // eod
};

void UpdateDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        UpdateDialog *_t = static_cast<UpdateDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendUpdatedSignal((*reinterpret_cast< Book*(*)>(_a[1]))); break;
        case 1: _t->sendStorage((*reinterpret_cast< Storage*(*)>(_a[1])),(*reinterpret_cast< Book*(*)>(_a[2]))); break;
        case 2: _t->on_pushButton_clicked(); break;
        case 3: _t->on_pushButton_2_clicked(); break;
        case 4: _t->receiveBookToEdit((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])),(*reinterpret_cast< Storage*(*)>(_a[2]))); break;
        case 5: _t->on_insert_button_clicked(); break;
        case 6: _t->on_delete_button_clicked(); break;
        case 7: _t->on_listWidget_itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 8: _t->receiveupdatedAuthorList((*reinterpret_cast< Storage*(*)>(_a[1]))); break;
        case 9: _t->on_pushButton_3_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (UpdateDialog::*_t)(Book * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&UpdateDialog::sendUpdatedSignal)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (UpdateDialog::*_t)(Storage * , Book * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&UpdateDialog::sendStorage)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject UpdateDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_UpdateDialog.data,
      qt_meta_data_UpdateDialog,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *UpdateDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *UpdateDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_UpdateDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int UpdateDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void UpdateDialog::sendUpdatedSignal(Book * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void UpdateDialog::sendStorage(Storage * _t1, Book * _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
