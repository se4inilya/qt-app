/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../src/course_work/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[29];
    char stringdata0[472];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 20), // "sendSignalForEdition"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(4, 50, 8), // "Storage*"
QT_MOC_LITERAL(5, 59, 24), // "on_update_button_clicked"
QT_MOC_LITERAL(6, 84, 23), // "on_actionOpen_triggered"
QT_MOC_LITERAL(7, 108, 25), // "on_actionCreate_triggered"
QT_MOC_LITERAL(8, 134, 25), // "on_listWidget_itemClicked"
QT_MOC_LITERAL(9, 160, 4), // "item"
QT_MOC_LITERAL(10, 165, 24), // "on_delete_button_clicked"
QT_MOC_LITERAL(11, 190, 24), // "on_create_button_clicked"
QT_MOC_LITERAL(12, 215, 12), // "receive_Book"
QT_MOC_LITERAL(13, 228, 5), // "Book*"
QT_MOC_LITERAL(14, 234, 4), // "book"
QT_MOC_LITERAL(15, 239, 20), // "receive_Updated_Book"
QT_MOC_LITERAL(16, 260, 16), // "receive_UserData"
QT_MOC_LITERAL(17, 277, 4), // "User"
QT_MOC_LITERAL(18, 282, 4), // "user"
QT_MOC_LITERAL(19, 287, 16), // "updateListWidget"
QT_MOC_LITERAL(20, 304, 4), // "flag"
QT_MOC_LITERAL(21, 309, 3), // "str"
QT_MOC_LITERAL(22, 313, 27), // "on_search_field_textChanged"
QT_MOC_LITERAL(23, 341, 4), // "arg1"
QT_MOC_LITERAL(24, 346, 26), // "on_previous_button_clicked"
QT_MOC_LITERAL(25, 373, 22), // "on_next_button_clicked"
QT_MOC_LITERAL(26, 396, 25), // "on_actionImport_triggered"
QT_MOC_LITERAL(27, 422, 25), // "on_actionExport_triggered"
QT_MOC_LITERAL(28, 448, 23) // "on_actionExit_triggered"

    },
    "MainWindow\0sendSignalForEdition\0\0"
    "QListWidgetItem*\0Storage*\0"
    "on_update_button_clicked\0"
    "on_actionOpen_triggered\0"
    "on_actionCreate_triggered\0"
    "on_listWidget_itemClicked\0item\0"
    "on_delete_button_clicked\0"
    "on_create_button_clicked\0receive_Book\0"
    "Book*\0book\0receive_Updated_Book\0"
    "receive_UserData\0User\0user\0updateListWidget\0"
    "flag\0str\0on_search_field_textChanged\0"
    "arg1\0on_previous_button_clicked\0"
    "on_next_button_clicked\0on_actionImport_triggered\0"
    "on_actionExport_triggered\0"
    "on_actionExit_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   99,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,  104,    2, 0x08 /* Private */,
       6,    0,  105,    2, 0x08 /* Private */,
       7,    0,  106,    2, 0x08 /* Private */,
       8,    1,  107,    2, 0x08 /* Private */,
      10,    0,  110,    2, 0x08 /* Private */,
      11,    0,  111,    2, 0x08 /* Private */,
      12,    1,  112,    2, 0x08 /* Private */,
      15,    1,  115,    2, 0x08 /* Private */,
      16,    1,  118,    2, 0x08 /* Private */,
      19,    2,  121,    2, 0x08 /* Private */,
      22,    1,  126,    2, 0x08 /* Private */,
      24,    0,  129,    2, 0x08 /* Private */,
      25,    0,  130,    2, 0x08 /* Private */,
      26,    0,  131,    2, 0x08 /* Private */,
      27,    0,  132,    2, 0x08 /* Private */,
      28,    0,  133,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 4,    2,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void, 0x80000000 | 17,   18,
    QMetaType::Void, QMetaType::Bool, QMetaType::QString,   20,   21,
    QMetaType::Void, QMetaType::QString,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendSignalForEdition((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])),(*reinterpret_cast< Storage*(*)>(_a[2]))); break;
        case 1: _t->on_update_button_clicked(); break;
        case 2: _t->on_actionOpen_triggered(); break;
        case 3: _t->on_actionCreate_triggered(); break;
        case 4: _t->on_listWidget_itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 5: _t->on_delete_button_clicked(); break;
        case 6: _t->on_create_button_clicked(); break;
        case 7: _t->receive_Book((*reinterpret_cast< Book*(*)>(_a[1]))); break;
        case 8: _t->receive_Updated_Book((*reinterpret_cast< Book*(*)>(_a[1]))); break;
        case 9: _t->receive_UserData((*reinterpret_cast< User(*)>(_a[1]))); break;
        case 10: _t->updateListWidget((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 11: _t->on_search_field_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: _t->on_previous_button_clicked(); break;
        case 13: _t->on_next_button_clicked(); break;
        case 14: _t->on_actionImport_triggered(); break;
        case 15: _t->on_actionExport_triggered(); break;
        case 16: _t->on_actionExit_triggered(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< User >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (MainWindow::*_t)(QListWidgetItem * , Storage * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::sendSignalForEdition)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::sendSignalForEdition(QListWidgetItem * _t1, Storage * _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
