/********************************************************************************
** Form generated from reading UI file 'updatedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UPDATEDIALOG_H
#define UI_UPDATEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UpdateDialog
{
public:
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *insert_button;
    QPushButton *delete_button;
    QListWidget *listWidget;
    QLabel *label_4;
    QWidget *formLayoutWidget_2;
    QFormLayout *formLayout_2;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *author_label;
    QLabel *yearofbirth_label;
    QLabel *yearofdeath_label;
    QLabel *picture_label;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLineEdit *lineEdit_2;
    QSpinBox *spinBox;
    QLabel *label_3;
    QLabel *label_2;
    QLabel *label;
    QLineEdit *lineEdit;
    QPushButton *pushButton_3;

    void setupUi(QDialog *UpdateDialog)
    {
        if (UpdateDialog->objectName().isEmpty())
            UpdateDialog->setObjectName(QStringLiteral("UpdateDialog"));
        UpdateDialog->resize(918, 445);
        pushButton = new QPushButton(UpdateDialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(50, 390, 89, 25));
        pushButton_2 = new QPushButton(UpdateDialog);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(780, 390, 89, 25));
        insert_button = new QPushButton(UpdateDialog);
        insert_button->setObjectName(QStringLiteral("insert_button"));
        insert_button->setGeometry(QRect(310, 290, 89, 25));
        delete_button = new QPushButton(UpdateDialog);
        delete_button->setObjectName(QStringLiteral("delete_button"));
        delete_button->setGeometry(QRect(460, 290, 89, 25));
        listWidget = new QListWidget(UpdateDialog);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(30, 150, 256, 192));
        label_4 = new QLabel(UpdateDialog);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(370, 160, 121, 17));
        formLayoutWidget_2 = new QWidget(UpdateDialog);
        formLayoutWidget_2->setObjectName(QStringLiteral("formLayoutWidget_2"));
        formLayoutWidget_2->setGeometry(QRect(300, 190, 251, 81));
        formLayout_2 = new QFormLayout(formLayoutWidget_2);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setContentsMargins(0, 0, 0, 0);
        label_5 = new QLabel(formLayoutWidget_2);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_5);

        label_6 = new QLabel(formLayoutWidget_2);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_6);

        label_7 = new QLabel(formLayoutWidget_2);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_7);

        author_label = new QLabel(formLayoutWidget_2);
        author_label->setObjectName(QStringLiteral("author_label"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, author_label);

        yearofbirth_label = new QLabel(formLayoutWidget_2);
        yearofbirth_label->setObjectName(QStringLiteral("yearofbirth_label"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, yearofbirth_label);

        yearofdeath_label = new QLabel(formLayoutWidget_2);
        yearofdeath_label->setObjectName(QStringLiteral("yearofdeath_label"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, yearofdeath_label);

        picture_label = new QLabel(UpdateDialog);
        picture_label->setObjectName(QStringLiteral("picture_label"));
        picture_label->setGeometry(QRect(610, 60, 291, 291));
        gridLayoutWidget = new QWidget(UpdateDialog);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(90, 10, 501, 131));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        lineEdit_2 = new QLineEdit(gridLayoutWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));

        gridLayout->addWidget(lineEdit_2, 2, 1, 1, 1);

        spinBox = new QSpinBox(gridLayoutWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));

        gridLayout->addWidget(spinBox, 1, 1, 1, 1);

        label_3 = new QLabel(gridLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        label_2 = new QLabel(gridLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 2, 0, 1, 1);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        lineEdit = new QLineEdit(gridLayoutWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        gridLayout->addWidget(lineEdit, 0, 1, 1, 1);

        pushButton_3 = new QPushButton(gridLayoutWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        gridLayout->addWidget(pushButton_3, 2, 2, 1, 1);


        retranslateUi(UpdateDialog);

        QMetaObject::connectSlotsByName(UpdateDialog);
    } // setupUi

    void retranslateUi(QDialog *UpdateDialog)
    {
        UpdateDialog->setWindowTitle(QApplication::translate("UpdateDialog", "Updating Menu", Q_NULLPTR));
        pushButton->setText(QApplication::translate("UpdateDialog", "OK", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("UpdateDialog", "Cancel", Q_NULLPTR));
        insert_button->setText(QApplication::translate("UpdateDialog", "Insert", Q_NULLPTR));
        delete_button->setText(QApplication::translate("UpdateDialog", "Delete", Q_NULLPTR));
        label_4->setText(QApplication::translate("UpdateDialog", "Selected author :", Q_NULLPTR));
        label_5->setText(QApplication::translate("UpdateDialog", "Author name :", Q_NULLPTR));
        label_6->setText(QApplication::translate("UpdateDialog", "Year of birth :", Q_NULLPTR));
        label_7->setText(QApplication::translate("UpdateDialog", "Year of death :", Q_NULLPTR));
        author_label->setText(QString());
        yearofbirth_label->setText(QString());
        yearofdeath_label->setText(QString());
        picture_label->setText(QString());
        label_3->setText(QApplication::translate("UpdateDialog", "Character capacity :", Q_NULLPTR));
        label_2->setText(QApplication::translate("UpdateDialog", "Picture path :", Q_NULLPTR));
        label->setText(QApplication::translate("UpdateDialog", "Book name :", Q_NULLPTR));
        pushButton_3->setText(QApplication::translate("UpdateDialog", "Overview", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class UpdateDialog: public Ui_UpdateDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UPDATEDIALOG_H
