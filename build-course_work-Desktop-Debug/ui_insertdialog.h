/********************************************************************************
** Form generated from reading UI file 'insertdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INSERTDIALOG_H
#define UI_INSERTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_InsertDialog
{
public:
    QListWidget *listWidget;
    QPushButton *pushButton;
    QPushButton *pushButton_2;

    void setupUi(QDialog *InsertDialog)
    {
        if (InsertDialog->objectName().isEmpty())
            InsertDialog->setObjectName(QStringLiteral("InsertDialog"));
        InsertDialog->setWindowModality(Qt::WindowModal);
        InsertDialog->resize(537, 425);
        listWidget = new QListWidget(InsertDialog);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(70, 40, 391, 301));
        pushButton = new QPushButton(InsertDialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(50, 370, 89, 25));
        pushButton_2 = new QPushButton(InsertDialog);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(400, 370, 89, 25));

        retranslateUi(InsertDialog);

        QMetaObject::connectSlotsByName(InsertDialog);
    } // setupUi

    void retranslateUi(QDialog *InsertDialog)
    {
        InsertDialog->setWindowTitle(QApplication::translate("InsertDialog", "Inserting Dialog", Q_NULLPTR));
        pushButton->setText(QApplication::translate("InsertDialog", "OK", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("InsertDialog", "Cancel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class InsertDialog: public Ui_InsertDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INSERTDIALOG_H
