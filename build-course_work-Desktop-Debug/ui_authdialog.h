/********************************************************************************
** Form generated from reading UI file 'authdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUTHDIALOG_H
#define UI_AUTHDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AuthDialog
{
public:
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label_2;
    QLabel *label_3;
    QLineEdit *username_line;
    QLineEdit *password_line;
    QPushButton *OK_button;
    QPushButton *Cancel_button;
    QLabel *err_label;
    QPushButton *pushButton;

    void setupUi(QDialog *AuthDialog)
    {
        if (AuthDialog->objectName().isEmpty())
            AuthDialog->setObjectName(QStringLiteral("AuthDialog"));
        AuthDialog->resize(364, 208);
        formLayoutWidget = new QWidget(AuthDialog);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(20, 20, 321, 71));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_2);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_3);

        username_line = new QLineEdit(formLayoutWidget);
        username_line->setObjectName(QStringLiteral("username_line"));
        username_line->setClearButtonEnabled(true);

        formLayout->setWidget(0, QFormLayout::FieldRole, username_line);

        password_line = new QLineEdit(formLayoutWidget);
        password_line->setObjectName(QStringLiteral("password_line"));
        password_line->setEnabled(true);
        password_line->setTabletTracking(true);
        password_line->setContextMenuPolicy(Qt::ActionsContextMenu);
        password_line->setAutoFillBackground(false);
        password_line->setEchoMode(QLineEdit::Password);
        password_line->setDragEnabled(false);
        password_line->setClearButtonEnabled(true);

        formLayout->setWidget(1, QFormLayout::FieldRole, password_line);

        OK_button = new QPushButton(AuthDialog);
        OK_button->setObjectName(QStringLiteral("OK_button"));
        OK_button->setGeometry(QRect(20, 160, 89, 25));
        Cancel_button = new QPushButton(AuthDialog);
        Cancel_button->setObjectName(QStringLiteral("Cancel_button"));
        Cancel_button->setGeometry(QRect(250, 160, 89, 25));
        err_label = new QLabel(AuthDialog);
        err_label->setObjectName(QStringLiteral("err_label"));
        err_label->setGeometry(QRect(10, 100, 341, 41));
        pushButton = new QPushButton(AuthDialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(140, 160, 89, 25));

        retranslateUi(AuthDialog);

        QMetaObject::connectSlotsByName(AuthDialog);
    } // setupUi

    void retranslateUi(QDialog *AuthDialog)
    {
        AuthDialog->setWindowTitle(QApplication::translate("AuthDialog", "Authentication", Q_NULLPTR));
        label_2->setText(QApplication::translate("AuthDialog", "Username :", Q_NULLPTR));
        label_3->setText(QApplication::translate("AuthDialog", "Password :", Q_NULLPTR));
        OK_button->setText(QApplication::translate("AuthDialog", "OK", Q_NULLPTR));
        Cancel_button->setText(QApplication::translate("AuthDialog", "Cancel", Q_NULLPTR));
        err_label->setText(QString());
        pushButton->setText(QApplication::translate("AuthDialog", "Sign up", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class AuthDialog: public Ui_AuthDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUTHDIALOG_H
