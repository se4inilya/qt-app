#pragma once
#include <string>
#include <qmetatype.h>

struct User
{
    int id;
    std::string username;
    std::string password_hash;
};

Q_DECLARE_METATYPE(User)

