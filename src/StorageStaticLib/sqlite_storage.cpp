#include "sqlite_storage.h"

#include <QtSql>
#include <QCryptographicHash>
#include <QDebug>

SqliteStorage::SqliteStorage(const string & dir_name) : dir_name_(dir_name){
    db_ = QSqlDatabase::addDatabase("QSQLITE");
}

bool SqliteStorage::open(){
    QString path = QString::fromStdString(this->dir_name_);
    db_.setDatabaseName(path);    // set sqlite database file path
    bool connected = db_.open();  // open db connection
    if (!connected) {
      //
        qDebug() << "Can not open database.";
        return false;
      //
    }
    return true;
}

Book getBookFromQuery(const QSqlQuery & query){
    Book b;
    b.id = query.value("id").toInt();
    b.book_name = query.value("book_name").toString().toStdString();
    b.picture_path = query.value("picture_path").toString().toStdString();
    b.character_capacity = query.value("character_capacity").toInt();
    b.user_id = query.value("user_id").toInt();
    return b;
}

Author getAuthorFromQuery(const QSqlQuery & query){
    Author a;
    a.id = query.value("id").toInt();
    a.authorName = query.value("authorName").toString().toStdString();
    a.yearOfBirth = query.value("yearOfBirth").toInt();
    a.yearOfDeath = query.value("yearOfDeath").toInt();
    a.author_picture = query.value("author_picture").toString().toStdString();
    return a;
}

bool SqliteStorage::close(){
    db_.close();
    return true;
}
// books
vector<Book> SqliteStorage::getAllBooks(void){
    vector<Book> books;
    QSqlQuery query("SELECT * FROM books");
    while(query.next()){
        Book b = getBookFromQuery(query);
        books.push_back(b);
    }

    return books;
}
optional<Book> SqliteStorage::getBookById(int book_id){
    QSqlQuery query;
    query.prepare("SELECT * FROM books WHERE id = :id");
    query.bindValue(":id", book_id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get book error:" << query.lastError();
        return nullopt;
    }
    if (query.next()) {
        Book b = getBookFromQuery(query);
        return b;
    }
    else {
       qDebug() << " not found ";
       return nullopt;
    }

}
bool SqliteStorage::updateBook(const Book &book){
    QSqlQuery query;
    query.prepare("UPDATE books SET book_name = :book_name, picture_path = :picture_path, character_capacity = :character_capacity WHERE id = :id");
    query.bindValue(":book_name", QString::fromStdString(book.book_name));
    query.bindValue(":picture_path", QString::fromStdString(book.picture_path));
    query.bindValue(":character_capacity", book.character_capacity);
    query.bindValue(":id", book.id);
    if (!query.exec()){
        qDebug() << "updateBook error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0){
        return false;
    }
    return true;


}
bool SqliteStorage::removeBook(int book_id){
    QSqlQuery query;
    query.prepare("DELETE FROM books WHERE id = :id;");
    query.bindValue(":id", book_id);
    if (!query.exec()){
        qDebug() << "deleteBook error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0){
        return false;
    }
    QSqlQuery query1;
    query1.prepare("DELETE FROM links WHERE books_id = :id;");
    query1.bindValue(":id", book_id);
    if (!query1.exec()){
        qDebug() << "deleteBook error:" << query1.lastError();
        return false;
    }
    if(query1.numRowsAffected() == 0){
        return false;
    }

    return true;
}
int SqliteStorage::insertBook(const Book &book, int user_id){
    QSqlQuery query;
    query.prepare("INSERT INTO books (book_name, picture_path, character_capacity, user_id) VALUES (:book_name, :picture_path, :character_capacity, :user_id)");
    query.bindValue(":book_name", QString::fromStdString(book.book_name));
    query.bindValue(":picture_path", QString::fromStdString(book.picture_path));
    query.bindValue(":character_capacity", book.character_capacity);
    query.bindValue(":user_id", user_id);
    if (!query.exec()){
        qDebug() << "addBook error:"
                 << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
    return var.toInt();
}
// Authors
vector<Author> SqliteStorage::getAllAuthors(void){
    vector<Author> authors;
    QSqlQuery query("SELECT * FROM authors");
    while(query.next()){
        Author a = getAuthorFromQuery(query);
        authors.push_back(a);
    }

    return authors;
}
optional<Author> SqliteStorage::getAuthorById(int author_id){
    QSqlQuery query;
    query.prepare("SELECT * FROM authors WHERE id = :id");
    query.bindValue(":id", author_id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get author error:" << query.lastError();
        return nullopt;
    }
    if (query.next()) {
        Author a = getAuthorFromQuery(query);
        return a;
    }
    else {
       qDebug() << " not found ";
       return nullopt;
    }
}
bool SqliteStorage::updateAuthor(const Author &author){
    QSqlQuery query;
    query.prepare("UPDATE authors SET authorName = :authorName, yearOfBirth = :yearOfBirth, yearOfDeath = :yearOfDeath WHERE id = :id");
    query.bindValue(":authorName", QString::fromStdString(author.authorName));
    query.bindValue(":yearOfBirth", author.yearOfBirth);
    query.bindValue(":yearOfDeath", author.yearOfDeath);
    query.bindValue(":id", author.id);
    if (!query.exec()){
        qDebug() << "updateBook error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0){
        return false;
    }
    return true;
}
bool SqliteStorage::removeAuthor(int author_id){
    QSqlQuery query;
    query.prepare("DELETE FROM authors WHERE id = :id");
    query.bindValue(":id", author_id);
    if (!query.exec()){
        qDebug() << "deleteBook error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0){
        return false;
    }
    return true;
}
int SqliteStorage::insertAuthor(const Author &author){
    QSqlQuery query;
    query.prepare("INSERT INTO authors (authorName, yearOfBirth, yearOfDeath) VALUES (:authorName, :yearOfBirth, :yearOfDeath)");
    query.bindValue(":authorName", QString::fromStdString(author.authorName));
    query.bindValue(":yearOfBirth", author.yearOfBirth);
    query.bindValue(":yearOfDeath", author.yearOfDeath);
    if (!query.exec()){
        qDebug() << "addBook error:"
                 << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
    return var.toInt();
}

vector<Book> SqliteStorage::getAllUserBooks(int user_id){
    vector<Book> books;
    QSqlQuery query;
    query.prepare("SELECT * FROM books WHERE user_id = :user_id");
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
       qDebug() << "get allUserMuseum error:" << query.lastError();
       return books;
    }
    while(query.next()){
        Book b = getBookFromQuery(query);
        books.push_back(b);
    }

    return books;
}

QString hashPassword(QString const & pass) {
   QByteArray pass_ba = pass.toUtf8();
   QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
   QString pass_hash = QString(hash_ba.toHex());
   return pass_hash;
}

optional<User> SqliteStorage::getUserAuth(string & username, string & password){

       QSqlQuery query;
       query.prepare("SELECT * FROM users "
                     "WHERE username = :username AND password_hash = :password_hash;");
       query.bindValue(":username", QString::fromStdString(username));
       query.bindValue(":password_hash", hashPassword(QString::fromStdString(password)));
       if (!query.exec())
       {
           QSqlError error = query.lastError();
           throw error;
       }
       if (query.next())
       {
           User user;
           user.id = query.value("id").toInt();
           user.username = username;
           user.password_hash = hashPassword(QString::fromStdString(password)).toStdString();
           return user;
       }

    return nullopt;
}

optional<User> SqliteStorage::getSignedUpUser(string & username, string & password){
    QSqlQuery query;
    query.prepare("INSERT INTO users (username, password_hash) VALUES (:username, :password_hash);");
    query.bindValue(":username", QString::fromStdString(username));
    query.bindValue(":password_hash", hashPassword(QString::fromStdString(password)));
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        throw error;
    }
    if (query.next())
    {
        User user;
        user.id = query.value("id").toInt();
        user.username = username;
        user.password_hash = hashPassword(QString::fromStdString(password)).toStdString();
        return user;
    }

    return nullopt;
}

// links
vector<Author> SqliteStorage::getAllBookAuthors(int book_id){
    vector<Author> authors;
    QSqlQuery query;
    query.prepare("SELECT * FROM links "
                  "WHERE  books_id = :books_id;");
    query.bindValue(":books_id", book_id);
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        throw error;
    }
    while (query.next())
    {
        optional<Author> a_opt = getAuthorById(query.value("authors_id").toInt());
        if (!a_opt)
        {
            qDebug() << "Error getting opt";
            continue;
        }
        Author a = a_opt.value();
        authors.push_back(a);
    }

    return authors;
}
bool SqliteStorage::insertBookAuthor(int book_id, int author_id){

    QSqlQuery query;
    query.prepare("INSERT INTO links (books_id, authors_id) VALUES (:book_id, :author_id)");
    query.bindValue(":book_id", book_id);
    query.bindValue(":author_id", author_id);
    if (!query.exec()){
        qDebug() << "deleteAuthorfromBook error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0){
        return false;
    }
    return true;
}
bool SqliteStorage::removeBookAuthor(int book_id, int author_id){
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE books_id = :book_id AND authors_id = :author_id;");
    query.bindValue(":book_id", book_id);
    query.bindValue(":author_id", author_id);
    if (!query.exec()){
        qDebug() << "deleteAuthorfromBook error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0){
        return false;
    }
    return true;
}

User getUserFromQuery(const QSqlQuery & query){
    User u;
    u.id = query.value("id").toInt();
    u.username = query.value("username").toString().toStdString();
    u.password_hash = query.value("password_hash").toString().toStdString();

    return u;
}

vector<User> SqliteStorage::getAllUsers(){
    vector<User> users;
    QSqlQuery query;
    query.prepare("SELECT * FROM users");

    if (!query.exec())
    {
       qDebug() << "get allUserBook error:" << query.lastError();
       return users;
    }
    while(query.next()){
        User u = getUserFromQuery(query);
        users.push_back(u);
    }

    return users;
}

vector<Book> SqliteStorage::takePageFromStorage(int page_number, int user_id){
    vector<Book> books;
    QSqlQuery query;
    query.prepare("SELECT * FROM books WHERE user_id = :user_id LIMIT :page_size OFFSET :skipped_items");

    int page_size = 4;
    int skipped_items = (page_number - 1) * page_size;

    query.bindValue(":user_id", user_id);
    query.bindValue(":page_size", page_size);
    query.bindValue(":skipped_items", skipped_items);
    qDebug() << skipped_items;
    if (!query.exec())
    {
       qDebug() << "get allUserBook error:" << query.lastError();
       return books;
    }
    while(query.next()){
        Book b = getBookFromQuery(query);
        qDebug() << QString::fromStdString(b.book_name);
        books.push_back(b);
    }

    return books;
}

int SqliteStorage::PagesCounter(int user_id){
    int counter = 0;
    QSqlQuery query;
    query.prepare("SELECT COUNT(*) FROM books WHERE user_id = :user_id");
    query.bindValue(":user_id", user_id);

    if (!query.exec())
    {
       qDebug() << "get allUserBook error:" << query.lastError();
       return 0;
    }
    while(query.next()){
        counter = query.value("COUNT(*)").toInt();
    }
    return counter;
}

vector<Book> SqliteStorage::search(string search, int user_id, int page_number){
    vector<Book> books;
    QSqlQuery query;
    query.prepare("SELECT * FROM books WHERE user_id = :user_id AND book_name LIKE ('%' || :search_text || '%') LIMIT :page_size OFFSET :skipped_items");

    int page_size = 4;
    int skipped_items = (page_number - 1) * page_size;

    query.bindValue(":user_id", user_id);
    query.bindValue(":search_text", QString::fromStdString(search));
    query.bindValue(":page_size", page_size);
    query.bindValue(":skipped_items", skipped_items);
    qDebug() << skipped_items;
    if (!query.exec())
    {
       qDebug() << "get allUserBook error:" << query.lastError();
       return books;
    }
    while(query.next()){
        Book b = getBookFromQuery(query);
        qDebug() << QString::fromStdString(b.book_name);
        books.push_back(b);
    }

    return books;
}

int SqliteStorage::SearchCounter(int user_id, string search){
    int counter = 0;
    QSqlQuery query;
    query.prepare("SELECT COUNT(*) FROM books WHERE user_id = :user_id AND book_name LIKE ('%' || :search_text || '%')");
    query.bindValue(":user_id", user_id);
    query.bindValue(":search_text", QString::fromStdString(search));
    if (!query.exec())
    {
       qDebug() << "get allUserBook error:" << query.lastError();
       return 0;
    }
    while(query.next()){
        counter = query.value("COUNT(*)").toInt();
    }
    return counter;
}


