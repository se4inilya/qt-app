#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H

#include "storage.h"
#include <QSqlDatabase>

class SqliteStorage : public Storage
{
    const string dir_name_;
    QSqlDatabase db_;

public:
    SqliteStorage(const string & dir_name);

    bool open();
    bool close();
    // books
    vector<Book> getAllBooks(void);
    optional<Book> getBookById(int book_id);
    bool updateBook(const Book &book);
    bool removeBook(int book_id);
    int insertBook(const Book &book, int user_id);
    vector<Book> getAllUserBooks(int user_id);
    // Authors
    vector<Author> getAllAuthors(void);
    optional<Author> getAuthorById(int author_id);
    bool updateAuthor(const Author &author);
    bool removeAuthor(int author_id);
    int insertAuthor(const Author &author);
    // users
       optional<User> getUserAuth(string & username, string & password);
       // links
       vector<Author> getAllBookAuthors(int student_id);
       bool insertBookAuthor(int student_id, int course_id);
       bool removeBookAuthor(int student_id, int course_id);
       optional<User> getSignedUpUser(string & username, string & password);
       vector<User> getAllUsers();
       vector<Book> takePageFromStorage(int page_number, int user_id);
       int PagesCounter(int user_id);
       int SearchCounter(int user_id, string search);
       vector<Book> search(string search, int user_id, int page_number);
};

#endif // SQLITE_STORAGE_H
