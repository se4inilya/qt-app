#pragma once

#include <vector>
#include <string>

#include "optional.h"
#include "book.h"
#include "author.h"
#include "csv.h"
#include "user.h"

using std::string;
using std::vector;

class Storage
{
 public:
    virtual ~Storage(){}

   virtual bool open() = 0;
   virtual bool close() = 0;
   // books
   virtual vector<Book> getAllBooks(void) = 0;
   virtual optional<Book> getBookById(int book_id) = 0;
   virtual bool updateBook(const Book &book) = 0;
   virtual bool removeBook(int book_id) = 0;
   virtual int insertBook(const Book &book, int user_id) = 0;
   virtual vector<Book> getAllUserBooks(int user_id) = 0;
   // Authors
   virtual vector<Author> getAllAuthors(void) = 0;
   virtual optional<Author> getAuthorById(int author_id) = 0;
   virtual bool updateAuthor(const Author &author) = 0;
   virtual bool removeAuthor(int author_id) = 0;
   virtual int insertAuthor(const Author &author) = 0;
    // users
       virtual optional<User> getUserAuth(string & username, string & password) = 0;
       // links
       virtual vector<Author> getAllBookAuthors(int book_id) = 0;
       virtual bool insertBookAuthor(int book_id, int author_id) = 0;
       virtual bool removeBookAuthor(int book_id, int author_id) = 0;
    virtual optional<User> getSignedUpUser(string & username, string & password) = 0;
    virtual vector<User> getAllUsers() = 0;
    virtual vector<Book> takePageFromStorage(int page_number, int user_id) = 0;
    virtual int PagesCounter(int user_id) = 0;
    virtual int SearchCounter(int user_id, string search) = 0;
    virtual vector<Book> search(string search, int user_id, int page_number) = 0;


};
