#pragma once
#include <string>
#include <qmetaobject.h>

struct Author
{
    int id;
    std::string authorName;
    int yearOfBirth;
    int yearOfDeath;
    std::string author_picture;
};

Q_DECLARE_METATYPE(Author)
