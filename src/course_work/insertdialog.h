#ifndef INSERTDIALOG_H
#define INSERTDIALOG_H

#include <QDialog>
#include "storage.h"
#include <QListWidget>

namespace Ui {
class InsertDialog;
}

class InsertDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InsertDialog(QWidget *parent = 0);
    ~InsertDialog();
signals:

    void sendupdatedAuthorList(Storage *);
private slots:
    void receiveStorage(Storage *, Book *);
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_listWidget_clicked(const QModelIndex &index);

private:
    Ui::InsertDialog *ui;

    Storage * storage_;
    Book * book_;
};

#endif // INSERTDIALOG_H
