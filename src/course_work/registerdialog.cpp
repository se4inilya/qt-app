#include "registerdialog.h"
#include "ui_registerdialog.h"
#include "sqlite_storage.h"

RegisterDialog::RegisterDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RegisterDialog)
{
    ui->setupUi(this);
}

RegisterDialog::~RegisterDialog()
{
    delete ui;
}

void RegisterDialog::on_pushButton_clicked()
{
    std::string dir_name = "../src/course_work/data/sql/data1";
    Storage * storage = new SqliteStorage(dir_name);
    if(!storage->open()){
        qDebug("storage was not opened");
        return;
    }
    string username = ui->lineEdit->text().toStdString();
    string password = ui->lineEdit_2->text().toStdString();
    string conf_password = ui->lineEdit_3->text().toStdString();
    if(username.empty() || password.empty() || conf_password.empty()){
        ui->error_label->setText("One of fields is empty. Fill it to continue.");
    }
    else{
    vector<User> users = storage->getAllUsers();

    bool flag = 0;
    for(int i = 0; i < users.size(); i++){
        if(username == users[i].username)
            flag = 1;
    }
    if(flag == 1){
        ui->error_label->setText("User with this username have existed yet.\nCome up with new one.");
        ui->lineEdit->clear();
        ui->lineEdit_2->clear();
        ui->lineEdit_3->clear();
    }
    else{
        if(password == conf_password){
            optional<User> useropt = storage->getSignedUpUser(username, password);

            delete storage;
            RegisterDialog::close();
            return;
        }
        else{
            ui->error_label->setText("Password was not confirmed. Try again.");
            ui->lineEdit->clear();
            ui->lineEdit_2->clear();
            ui->lineEdit_3->clear();
        }
    }
    }
}

void RegisterDialog::on_pushButton_2_clicked()
{
    RegisterDialog::close();
}
