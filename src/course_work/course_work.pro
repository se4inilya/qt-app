QT += sql
#-------------------------------------------------
#
# Project created by QtCreator 2019-06-03T16:04:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = course_work
TEMPLATE = app

INCLUDEPATH += ../StorageStaticLib
LIBS += ../build-StorageStaticLib-Desktop-Debug/libStorageStaticLib.a

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    adddialog.cpp \
    authdialog.cpp \
    insertdialog.cpp \
    registerdialog.cpp \
    updatedialog.cpp

HEADERS += \
        mainwindow.h \
    adddialog.h \
    authdialog.h \
    insertdialog.h \
    registerdialog.h \
    updatedialog.h \

FORMS += \
        mainwindow.ui \
    adddialog.ui \
    authdialog.ui \
    insertdialog.ui \
    registerdialog.ui \
    updatedialog.ui

DISTFILES += \
    data/sql/data1 \
    data/xml/authors.xml \
    data/xml/books.xml \
    data/xmlauthors.xml \
    data/pictures/1.jpg \
    data/pictures/2.jpg \
    data/pictures/3.jpg \
    data/pictures/4.jpg \
    data/pictures/5.jpg \
    data/pictures/6.jpg \
    data/pictures/book.jpg \
    data/csv/authors.csv \
    data/csv/books.csv \
    data/csv/new.csv
