#include "insertdialog.h"
#include "ui_insertdialog.h"

#include "QListWidget"
#include <QDebug>

InsertDialog::InsertDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InsertDialog)
{
    ui->setupUi(this);
    ui->pushButton->setEnabled(false);
}

InsertDialog::~InsertDialog()
{
    delete ui;
}

void InsertDialog::receiveStorage(Storage * storage, Book * book){
    storage_ = storage;
    book_ = book;

    vector<Author> authors = storage_->getAllAuthors();
    vector<Author> book_authors = storage->getAllBookAuthors(book_->id);
    for(int i = 0; i < authors.size(); i++){
        int flag = 0;
        for(int j = 0; j < book_authors.size(); j++){
            if(authors.at(i).id == book_authors.at(j).id){
                flag = 1;
            }
        }
        if(flag == 1)
            continue;
        QListWidget *listWidget = ui->listWidget;
        QListWidgetItem *authorItem = new QListWidgetItem;

        QVariant qvar;
        qvar.setValue(authors.at(i));
        QString name = QString::fromStdString(authors.at(i).authorName);
        authorItem->setText(name);
        authorItem->setData(Qt::UserRole, qvar);

        listWidget->addItem(authorItem);
    }
}

void InsertDialog::on_pushButton_clicked()
{
    QListWidgetItem * item = ui->listWidget->selectedItems().at(0);
    QVariant variant = item->data(Qt::UserRole);
    Author a = variant.value<Author>();
    if(!storage_->insertBookAuthor(book_->id, a.id))
        qDebug() << "Can't insert author";

    emit(sendupdatedAuthorList(storage_));
    InsertDialog::close();
}

void InsertDialog::on_pushButton_2_clicked()
{
    InsertDialog::close();
}

void InsertDialog::on_listWidget_clicked(const QModelIndex &index)
{
    ui->pushButton->setEnabled(true);
}
