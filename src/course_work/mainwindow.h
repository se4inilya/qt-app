#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidget>
#include <QDebug>
#include <QFileDialog>
#include <QString>
#include <QDialog>
#include <QtGui>
#include <string.h>
#include <fstream>
#include <sstream>
#include <stdio.h>

#include "storage.h"
#include "csv_storage.h"
#include "sqlite_storage.h"
#include "adddialog.h"
#include "updatedialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void loadBook(string & dir_name);
    ~MainWindow();
signals:
    void sendSignalForEdition(QListWidgetItem *, Storage *);
private slots:
    void on_update_button_clicked();

    void on_actionOpen_triggered();

    void on_actionCreate_triggered();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void on_delete_button_clicked();

    void on_create_button_clicked();

    void receive_Book(Book * book);

    void receive_Updated_Book(Book * book);

    void receive_UserData(User user);

    void updateListWidget(bool flag, QString str);

    void on_search_field_textChanged(const QString &arg1);

    void on_previous_button_clicked();

    void on_next_button_clicked();

    void on_actionImport_triggered();

    void on_actionExport_triggered();

    void on_actionExit_triggered();

private:
    Ui::MainWindow *ui;
    User user_;

    AddDialog * add_dialog;
    UpdateDialog * update_dialog;

    Storage * storage_ = nullptr;
    QString file_name;
    int pageNumber;
    int searchPageNumber;
};

#endif // MAINWINDOW_H
