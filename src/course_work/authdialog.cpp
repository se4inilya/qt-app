#include "mainwindow.h"
#include "authdialog.h"
#include "ui_authdialog.h"


AuthDialog::AuthDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AuthDialog)
{
    ui->setupUi(this);
}

AuthDialog::~AuthDialog()
{
    delete ui;
}

void AuthDialog::on_OK_button_clicked()
{
    std::string dir_name = "../src/course_work/data/sql/data1";
    Storage * storage = new SqliteStorage(dir_name);
    if(!storage->open()){
        qDebug("storage was not opened");
        return;
    }
    string username = ui->username_line->text().toStdString();
    string password = ui->password_line->text().toStdString();
    optional<User> useropt = storage->getUserAuth(username, password);
    if(useropt != nullopt){
        qDebug("storage was not opened");
        mainwindow = new MainWindow;
        User user = useropt.value();
        connect(this, SIGNAL(sendUserData(User)), mainwindow, SLOT(receive_UserData(User)));
        emit(sendUserData(user));
        mainwindow->loadBook(dir_name);
        mainwindow->show();
        delete storage;
        AuthDialog::close();
        return;
    }
    else{
        ui->err_label->setText("Username or password was entered uncorrectly.\nTry again.");
        ui->username_line->clear();
        ui->password_line->clear();
    }
}

void AuthDialog::on_Cancel_button_clicked()
{
    AuthDialog::close();
}

void AuthDialog::on_pushButton_clicked()
{
    regdialog = new RegisterDialog(this);
    regdialog->show();
}
