#include "authdialog.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "adddialog.h"


#include <QDebug>
#include <QFileDialog>
#include <QDialog>
#include <QMessageBox>
#include <QSqlDatabase>


using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->create_button->setEnabled(false);
    ui->delete_button->setEnabled(false);
    ui->update_button->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete storage_;
    delete ui;
}

void MainWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(
                   this,              // parent
                   "Open file",  // caption
                   "/home/iluha/projects/progbase2/labs/lab9/data/sql/",                // directory to start with
                   "SQLite (*.sqlite);;All Files (*)");  // file name filter
       qDebug() << fileName;
       std::string name = fileName.toStdString();
       const char * namec = name.c_str();
       char c_name[100];
       int num_of_slash = 0;
       for (; *namec != '\0'; namec += 1)
       {
           if (*namec == '/')
               num_of_slash += 1;
       }
       namec = name.c_str();
       for (int i = 0;; i += 1)
       {
           c_name[i] = *namec;
           if (*namec == '/')
               num_of_slash -= 1;
           if (num_of_slash == 0)
           {
               c_name[i + 1] = '\0';
               break;
           }
           namec += 1;
       }
       fileName = c_name;
       if (fileName.size() == 0)
           return;

       qDebug() << "Path:" << fileName;
       QDir dir(fileName);
       if (!dir.exists())
       {
           qDebug() << "Wrong path:" << fileName;
           dir.mkpath(".");
           fileName = fileName + "new_data.sqlite";
           ofstream stor;
           stor.open(fileName.toStdString(), ios::out);
           stor.close();
       }
       else
           fileName = QString::fromStdString(name);

       if (storage_ != nullptr)
      {
           delete storage_;
           storage_ = nullptr;
          ui->listWidget->clear();
       }

       file_name = fileName;
       SqliteStorage *sql = new SqliteStorage(name);
       storage_ = sql;

       if(!storage_->open())
       {
           //cerr << "Can't load storage: " << fileName;
           //cerr << "This file doesn't exist" << endl;
           return;
       }

       vector<Book> books = storage_->getAllBooks();

       for(int i = 0; i < books.size(); i++){
           QListWidget *listWidget = ui->listWidget;
           QListWidgetItem *bookItem = new QListWidgetItem;

           QVariant qvar;
           qvar.setValue(books.at(i));
           QString name = QString::fromStdString(books.at(i).book_name);
           bookItem->setText(name);
           bookItem->setData(Qt::UserRole, qvar);

           listWidget->addItem(bookItem);
        }

     ui->create_button->setEnabled(true);
     ui->delete_button->setEnabled(false);
     ui->update_button->setEnabled(false);
}

void MainWindow::on_actionCreate_triggered()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString current_folder = QDir::currentPath();
    QString file_path = dialog.getSaveFileName(
                this,
                "Select folder for create",
                current_folder + "/new_data.sqlite",
                "Folders"
                );
    if (file_path.size() == 0)
        return;

    if (storage_ != nullptr)
    {
        delete storage_;
        storage_ = nullptr;

        ui->listWidget->clear();

        ui->update_button->setEnabled(false);
        ui->delete_button->setEnabled(false);
        ui->id_label->setText("-");
        ui->book_label->setText("-");
        ui->author_label->setText("-");
        ui->char_label->setText("-");
    }
    file_name = file_path;
    SqliteStorage * sql = new SqliteStorage(file_name.toStdString());

    storage_ = sql;
    ui->create_button->setEnabled(true);

    storage_->close();
}

void MainWindow::updateListWidget(bool flag, QString str){
    ui->listWidget->clear();
    vector<Book> books;

    if(flag == 0)
        books = storage_->takePageFromStorage(pageNumber, user_.id);
    else
        books = storage_->search(str.toStdString(), user_.id, pageNumber);

    for(int i = 0; i < books.size(); i++){
        QListWidget *listWidget = ui->listWidget;
        QListWidgetItem *bookItem = new QListWidgetItem;

        QVariant qvar;
        qvar.setValue(books.at(i));

        QString name = QString::fromStdString(books.at(i).book_name);
        bookItem->setText(name);
        bookItem->setData(Qt::UserRole, qvar);

        listWidget->addItem(bookItem);
     }

    int pages_total = 0;

    if(flag == 0){
        pages_total = storage_->PagesCounter(user_.id) / 4;
       if(storage_->PagesCounter(user_.id) % 4 != 0 || storage_->PagesCounter(user_.id) / 4 == 0)
           pages_total += 1;
    }
    else{
        pages_total = storage_->SearchCounter(user_.id, str.toStdString()) / 4;
        if(storage_->SearchCounter(user_.id, str.toStdString()) % 4 != 0 || storage_->SearchCounter(user_.id, str.toStdString()) / 4 == 0)
            pages_total += 1;
    }
    if(pageNumber == 1)
        ui->previous_button->setEnabled(false);
    else
        ui->previous_button->setEnabled(true);

    if(pages_total == pageNumber)
        ui->next_button->setEnabled(false);
    else
       ui->next_button->setEnabled(true);

        ui->page_label->clear();
    ui->page_label->setText("Page " + QString::fromStdString(to_string(pageNumber)) + " / " + QString::fromStdString(to_string(pages_total)));
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem *book)
{
    ui->delete_button->setEnabled(true);
    ui->update_button->setEnabled(true);

    QVariant variant = book->data(Qt::UserRole);
    Book b = variant.value<Book>();

    vector<Author> bookAuthors = storage_->getAllBookAuthors(b.id);

    string id = to_string(b.id);
    string char_cap = to_string(b.character_capacity);
    ui->id_label->setText(QString::fromStdString(id));
    ui->book_label->setText(QString::fromStdString(b.book_name));
    QString authornames = "";
    int counter = 0;
    for(int i = 0; i < bookAuthors.size(); i++){
        if(i == 0)
            authornames += QString::fromStdString(bookAuthors.at(i).authorName);
        else if(i == 1)
            authornames += ", " + QString::fromStdString(bookAuthors.at(i).authorName);
        else
            counter++;
    }
    if(counter != 0)
        authornames += ("+" + QString::fromStdString(to_string(counter)));
    ui->author_label->setText(authornames);
    ui->char_label->setText(QString::fromStdString(char_cap));
    QPixmap authPixMap(QString::fromStdString(b.picture_path));
    qDebug() << QString::fromStdString(b.picture_path);
    ui->label_6->setPixmap(authPixMap);
}



void MainWindow::on_delete_button_clicked()
{
    vector<Book> books = storage_->getAllBooks();
    if(books.empty()){
        return;
    }

    QMessageBox::StandardButton approve = QMessageBox::question(
            this,
            "Delete window",
            "Do you really want to delete this element ?",
            QMessageBox::Yes| QMessageBox::No
    );
    if (approve == QMessageBox::Yes) {
           qDebug() << "Yes was clicked";
       } else {
           qDebug() << "Yes was *not* clicked";
           return;
       }

    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();

    if(items.at(0) == NULL){
        return;
    }

    foreach(QListWidgetItem * item, items){
        items = ui->listWidget->selectedItems();
        QVariant var = item->data(Qt::UserRole);
        Book b = var.value<Book>();

        delete ui->listWidget->takeItem(ui->listWidget->row(item));

        if(!storage_->removeBook(b.id))
            qDebug() << "Was not deleted" << endl;
    }


    books = storage_->getAllBooks();

    bool flag = 0;
    for(int i = 0; i < books.size(); i++){
        if(user_.id == books[i].user_id)
            flag = 1;
    }
    if(flag == 0){
        ui->delete_button->setEnabled(false);
        ui->update_button->setEnabled(false);

        ui->id_label->setText("-");
        ui->book_label->setText("-");
        ui->author_label->setText("-");
        ui->char_label->setText("-");

        return;
    }


        updateListWidget(1, ui->search_field->text());
    books = storage_->takePageFromStorage(pageNumber, user_.id);
    if(books.size() == 0){
        MainWindow::on_previous_button_clicked();

        int pages_total = storage_->PagesCounter(user_.id) / 4;
        if(storage_->PagesCounter(user_.id) % 4 != 0)
            pages_total += 1;

        if(pages_total == pageNumber)
            ui->next_button->setEnabled(false);

    }

    ui->update_button->setEnabled(false);
    ui->delete_button->setEnabled(false);
    /*
        items = ui->listWidget->selectedItems();
        QListWidgetItem * book = items.at(0);

        QVariant variant = book->data(Qt::UserRole);
        Book b1 = variant.value<Book>();
    */

        ui->id_label->setText("-");
        ui->book_label->setText("-");
        ui->author_label->setText("-");
        ui->char_label->setText("-");


}

void MainWindow::on_create_button_clicked()
{
    add_dialog = new AddDialog(this);
    add_dialog->show();

    connect(add_dialog, SIGNAL(sendSignal(Book*)), this, SLOT(receive_Book(Book*)));

}

void MainWindow::on_update_button_clicked()
{
    update_dialog = new UpdateDialog(this);
    update_dialog->show();

    connect(this, SIGNAL(sendSignalForEdition(QListWidgetItem*, Storage *)), update_dialog, SLOT(receiveBookToEdit(QListWidgetItem*, Storage *)));
    connect(update_dialog, SIGNAL(sendUpdatedSignal(Book*)), this, SLOT(receive_Updated_Book(Book*)));

    QListWidgetItem * item = ui->listWidget->selectedItems().at(0);
    emit(sendSignalForEdition(item, storage_));


}

void MainWindow::receive_Book(Book *book){

    vector<Book> books1 = storage_->takePageFromStorage(pageNumber, user_.id);
    if(books1.size() == 4){
        storage_->insertBook(*book, user_.id);
        vector<Book> books = storage_->getAllBooks();
        int pages_total = storage_->PagesCounter(user_.id) / 4;
        if(storage_->PagesCounter(user_.id) % 4 != 0 || storage_->PagesCounter(user_.id) / 4 == 0)
            pages_total += 1;
//        while(pageNumber != pages_total)
//            MainWindow::on_next_button_clicked();


    }
    else{
        storage_->insertBook(*book, user_.id);
        vector<Book> books = storage_->getAllBooks();

    QListWidget * listw = ui->listWidget;
    QListWidgetItem *BookItem = new QListWidgetItem();

    QVariant qvar;
    qvar.setValue(books.at(books.size() - 1));

    QString name = QString::fromStdString(books.at(books.size() - 1).book_name);
    BookItem->setText(name);
    BookItem->setData(Qt::UserRole, qvar);

    listw->addItem(BookItem);
    }
    ui->update_button->setEnabled(false);
    ui->delete_button->setEnabled(false);
    updateListWidget(1, ui->search_field->text());
    //updateListWidget();
}

void MainWindow::receive_Updated_Book(Book *book){
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant qvar = item->data(Qt::UserRole);
    Book b = qvar.value<Book>();

    book->id = b.id;

    storage_->updateBook(*book);
    //storage_->close();

    QListWidgetItem * book_item = ui->listWidget->takeItem(ui->listWidget->row(item));

    QVariant var;
    var.setValue(*book);

    QString name = QString::fromStdString(book->book_name);
    book_item->setText(name);
    book_item->setData(Qt::UserRole, var);

    ui->listWidget->addItem(book_item);
    ui->update_button->setEnabled(false);
    ui->delete_button->setEnabled(false);

    updateListWidget(1, ui->search_field->text());
}

void MainWindow::receive_UserData(User user)
{
    user_.id = user.id;
    user_.username = user.username;
    user_.password_hash = user.password_hash;
}

void MainWindow::loadBook(string & dir_name){
    SqliteStorage *sql = new SqliteStorage(dir_name);
    storage_ = sql;

    if(!storage_->open())
    {
        qDebug() << "Can't load storage: " ;
        //cerr << "This file doesn't exist" << endl;
        return;
    }
    qDebug() << user_.id;
    int pages_total = storage_->PagesCounter(user_.id) / 4;
    if(storage_->PagesCounter(user_.id) % 4 != 0 || storage_->PagesCounter(user_.id) == 0)
        pages_total += 1;
    pageNumber = 1;
    vector<Book> books = storage_->takePageFromStorage(pageNumber, user_.id);


    for(int i = 0; i < books.size(); i++){
        QListWidget *listWidget = ui->listWidget;
        QListWidgetItem *bookItem = new QListWidgetItem;

        QVariant qvar;
        qvar.setValue(books.at(i));

        QString name = QString::fromStdString(books.at(i).book_name);
        bookItem->setText(name);
        bookItem->setData(Qt::UserRole, qvar);

        listWidget->addItem(bookItem);

     }

  ui->create_button->setEnabled(true);
  ui->delete_button->setEnabled(false);
  ui->update_button->setEnabled(false);
  ui->previous_button->setEnabled(false);
  if(pages_total == pageNumber)
      ui->next_button->setEnabled(false);
    ui->page_label->setText("Page " + QString::fromStdString(to_string(pageNumber)) + " / " + QString::fromStdString(to_string(pages_total)));
}


void MainWindow::on_search_field_textChanged(const QString &arg1)
{
    pageNumber = 1;
    updateListWidget(1, arg1);
}

void MainWindow::on_previous_button_clicked()
{
    pageNumber -= 1;
    updateListWidget(1, ui->search_field->text());
    ui->next_button->setEnabled(true);
    ui->update_button->setEnabled(false);
    ui->delete_button->setEnabled(false);

}

void MainWindow::on_next_button_clicked()
{
    pageNumber += 1;
    updateListWidget(1, ui->search_field->text());
    ui->previous_button->setEnabled(true);
    ui->update_button->setEnabled(false);
    ui->delete_button->setEnabled(false);
}



void MainWindow::on_actionImport_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(
                   this,              // parent
                   "Open file",  // caption
                   "../src/course_work/data/csv/",                // directory to start with
                   "CSV (*.csv);;All Files (*)");  // file name filter
       qDebug() << fileName;

    if(fileName.length() == 0)
        return;

    CsvStorage * csv_storage = new CsvStorage(fileName.toStdString());

    if(!csv_storage->open())
    {
        qDebug() << "can not open storage";
        QMessageBox::information(
            this,
            "Error",
            "Can not open storage");
        delete csv_storage;
        return;
    }

    vector<Book> books = csv_storage->getAllBooks();
    for(int i = 0; i < books.size(); i++){
        storage_->insertBook(books.at(i), user_.id);
    }

    updateListWidget(0, "");

    delete csv_storage;

}

void MainWindow::on_actionExport_triggered()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString current_folder = QDir::currentPath();
    QString file_path = dialog.getSaveFileName(
                this,
                "Select folder for create",
                current_folder + "/new.csv",
                "Folders"
                );
    if (file_path.size() == 0)
        return;

    CsvStorage * csv_storage = new CsvStorage(file_path.toStdString());
    vector<Book> books = storage_->getAllUserBooks(user_.id);
    for(int i = 0; i < books.size(); i++){
       csv_storage->insertBook(books.at(i), user_.id);
    }

    if(!csv_storage->close())
    {
        qDebug() << "can not open storage";
        QMessageBox::information(
            this,
            "Error",
            "Can not open storage");
        delete csv_storage;
        return;
    }
    delete csv_storage;
}

void MainWindow::on_actionExit_triggered()
{
    MainWindow::close();
}
